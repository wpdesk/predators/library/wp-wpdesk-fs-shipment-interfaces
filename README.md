[![pipeline status](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-wpdesk-fs-shipment-interfaces/commits/master)

wp-wpdesk-fs-shipment
====================
